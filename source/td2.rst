======================
TD requêtes imbriquées
======================

Le TD est dédiés aux requêtes imbriquées

Pour gérer ses stocks, un magasin s’est constitué une base de données dont le MCD vous est donné ci-dessous

.. image:: images/stock.svg

Le script de création de la base de données est le suivant::
	     
  CREATE TABLE CATEGORIE (idCat decimal(5,0), nomCat varchar(15), PRIMARY KEY (idCat));

  CREATE TABLE PRODUIT (refProd decimal(8,0),intituleProd varchar(15), prixUnitaireProd decimal(8,2), idCat decimal(5,0), PRIMARY KEY (refProd));

  CREATE TABLE CONTENIR (refProd decimal(8,0), idRayon decimal(3,0), Qte decimal(5,0), PRIMARY KEY (refProd, idRayon));

  CREATE TABLE RAYON (idRayon decimal(3,0), nomRayon varchar(15), PRIMARY KEY (idRayon));

  ALTER TABLE PRODUIT ADD FOREIGN KEY (idCat) REFERENCES CATEGORIE (idCat);
  ALTER TABLE CONTENIR ADD FOREIGN KEY (idRayon) REFERENCES RAYON (idRayon);
  ALTER TABLE CONTENIR ADD FOREIGN KEY (refProd) REFERENCES PRODUIT (refProd);

Les tests sont faits sur une l'instance de la base de données suivantes::

  +---------------------------------+   +---------------------+
  | RAYON                           |   | CATEGORIE           |
  +---------+-----------------------+   +-------+-------------+
  | idRayon | nomRayon              |   | idCat | nomCat      |
  +---------+-----------------------+   +-------+-------------+
  |       1 | Fournitures scolaires |   |     1 | Alimentaire |
  |       2 | Bazar                 |   |     2 | Bricolage   |
  |       3 | Soda et jus de fruits |   |     3 | Vaisselle   |
  |       4 | Produits frais        |   |     4 | Boisson     |
  |       5 | Poissonnerie          |   +-------+-------------+
  +---------+-----------------------+

  +--------------------------+   +-------------------------------------------------------+
  | CONTENIR                 |   | PRODUIT                                               |
  +---------+---------+------+   +---------+------------------+------------------+-------+
  | refProd | idRayon | Qte  |   | refProd | intituleProd     | prixUnitaireProd | idCat |
  +---------+---------+------+   +---------+------------------+------------------+-------+
  |       1 |       5 |   43 |   |       1 | Cabillaud        |            12.20 |     1 |
  |       2 |       5 |   12 |   |       2 | Crabe            |            14.40 |     1 |
  |       3 |       1 |  214 |   |       3 | Crayon de papier |             0.45 |     2 |
  |       3 |       2 |  102 |   |       4 | Marteau          |             4.80 |     2 |
  |       4 |       2 |   11 |   |       5 | Tournevis        |             2.50 |     2 |
  |       5 |       2 |    8 |   |       6 | Verre            |             1.10 |     3 |
  |       6 |       2 |   18 |   |       7 | Assiette         |             0.45 |     3 |
  |       7 |       2 |   24 |   |       8 | Couteau          |             1.25 |     3 |
  |       8 |       2 |   25 |   |       9 | Fourchette       |             1.00 |     3 |
  |      10 |       3 |   41 |   |      10 | Jus de pommes    |             1.20 |     4 |
  |      11 |       3 |   22 |   |      11 | Limonade         |             0.80 |     4 |
  |      12 |       1 |   10 |   |      12 | Cola             |             0.90 |     4 |
  |      12 |       2 |   14 |   |      13 | Perceuse         |            52.00 |     2 |
  |      12 |       3 |   14 |   +---------+------------------+------------------+-------+
  |      12 |       5 |   11 |
  +---------+---------+------+

	   
.. include::  exercices/imbriquees/exo1.rst
.. include::  exercices/imbriquees/exo2.rst
.. include::  exercices/imbriquees/exo3.rst
.. include::  exercices/imbriquees/exo4.rst
.. include::  exercices/imbriquees/exo5.rst
.. include::  exercices/imbriquees/exo6.rst

