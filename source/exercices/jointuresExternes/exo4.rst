Valeur des rayons avec les 0
----------------------------

Écrire une requête qui permet de connaître la valeur de chaque
    rayon en affichant les 0 appelée valTot. On les classera par ordre des numéros de rayon.

.. easypython:: exercices/jointuresExternes/exo4.sql
   :language: mysql
   :uuid: 1231313
   :titre: Valeur rayons avec 0
