Stock des produits avec 0
-------------------------

Écrire une requête permettant de connaître la quantité totale
    de chaque produit stockée dans le magasin en affichant les 0.. Le résultat comportera les informations du produit ainsi que la quantité stockée (appelée *qteTot*) et sera classé dans l'ordre décroissant des stocks. Si deux produits ont le mêm stocks, on les classera par ordre de référence.

.. easypython:: exercices/jointuresExternes/exo3.sql
   :language: mysql
   :uuid: 1231313
   :titre: Stock des produits avec 0
