Contenu des rayons
------------------

Ecrire une requête qui permet de compter le nombre de
produits stockés dans chaque rayon en affichant les 0. Le résultat comportera les informations du rayon ainsi que le nombre de produits stockés (appelé *nbProd*) et sera classé dans l'ordre décroissant des quantités stockées.

.. easypython:: exercices/jointuresExternes/exo1.sql
   :language: mysql
   :uuid: 1231313
   :titre: Stock avec 0
