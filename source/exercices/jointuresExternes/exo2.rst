Nb de rayons stockant un produit (avec les 0)
---------------------------------------------

Écrire une requête qui permet de compter pour chaque produit le
nombre de rayons où il est stocké (en comptant les 0). Le résultat comportera les informations du produit ainsi que le nombre de rayons où il est stocké (appelé *nbRayons*) et sera classé dans l'ordre croissant des références de produit.

.. easypython:: exercices/jointuresExternes/exo2.sql
   :language: mysql
   :uuid: 1231313
   :titre: Nb rayons 0
