Valeur d'un rayon
-------------------

Donner la valeur en Euros du rayon Bazar. Votre requête ne doit retourner qu'une colonne de nom *valTotale*


.. easypython:: exercices/agregats/exo3.sql
   :language: mysql
   :uuid: 1231313
   :titre: Valeur d'un rayon
