Produit Cher
-------------------

Donner le prix du produit le plus cher tous rayons confondus. Votre requête doit retourner une seule colonne de nom *prixCher*


.. easypython:: exercices/agregats/exo1.sql
   :language: mysql
   :uuid: 1231313
   :titre: Produit cher
