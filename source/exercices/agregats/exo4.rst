Produit le moins cher
---------------------


Donner le nom du produit le moins cher de base de données. S'il y en a plusieurs on veut les trier dans l'ordre alphabétique.

.. easypython:: exercices/agregats/exo4.sql
   :language: mysql
   :uuid: 1231313
   :titre: Produit pas cher
