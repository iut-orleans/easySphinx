Prix minimum produit alimentaire par rayon
------------------------------------------

Donner par rayon le prix maximum des produits alimentaires. On veut avoir l'identifiant du rayon, son nom et une colonne de nom *prixMax*. La liste doit être triée par identifiant de rayon.


.. easypython:: exercices/agregats/exo8.sql
   :language: mysql
   :uuid: 1231313
   :titre: Prix minimum alimentaire
