Rayons à plus de 100€
------------------------------------------

Donner la liste des noms de rayons contenant plus de 100€ de produits. La liste doit être triée dans l'ordre alphabétique.

.. easypython:: exercices/agregats/exo9.sql
   :language: mysql
   :uuid: 1231313
   :titre: Rayons à plus de 100
