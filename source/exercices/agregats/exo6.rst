Nombre de produits par rayon
----------------------------


 Donner pour chaque rayon le nombre de produits différents qu’il stocke. On veut l'identifiant et le nom du rayon ainsi que le nombre de produits (cette colonne doit s'appeler *nbProd*). La liste doit être triée par références.

.. easypython:: exercices/agregats/exo6.sql
   :language: mysql
   :uuid: 1231313
   :titre: Nombre produits par rayon
