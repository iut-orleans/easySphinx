Stock produits alimentaires
---------------------------

Donner la liste des produits alimentaires (référence et nom) dont le stock total est valorisé à plus 200€. Cette liste doit être triée par références.

.. easypython:: exercices/agregats/exo10.sql
   :language: mysql
   :uuid: 1231313
   :titre: Stock produits alimentaires
