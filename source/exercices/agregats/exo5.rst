Produit alimentaire le plus cher
--------------------------------


Donner le nom du produit alimentaire le plus cher. Si il y en a plusieurs, il faut trier les noms par ordre alphabétique.

.. easypython:: exercices/agregats/exo5.sql
   :language: mysql
   :uuid: 1231313
   :titre: Alimentaire cher
