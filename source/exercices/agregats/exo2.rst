Nombre de produits
-------------------

Donner le nombre des produits du rayon Bazar qui valent moins de 5€. Votre requête ne doit retourner qu'une colonne de nom *nbProd*


.. easypython:: exercices/agregats/exo2.sql
   :language: mysql
   :uuid: 1231313
   :titre: Nombre de produits
