Quantité stockée par produit
----------------------------

Pour chaque produit donner la quantité totale stockée dans le magasin. On veut la référence du produit, son nom et sa quantité stockée (cette colonne doit s'appeler *qteTotale*). La liste doit être triée dans l'ordre décroissant des quantités stockées.

.. easypython:: exercices/agregats/exo7.sql
   :language: mysql
   :uuid: 1231313
   :titre: stocks par produit
