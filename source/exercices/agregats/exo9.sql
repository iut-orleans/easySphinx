uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select nomRayon
	   from RAYON natural join CONTENIR natural join PRODUIT
	   group by idRayon, nomRayon
	   having sum(qte*prixUnitaireProd)>=100
	   order by nomRayon"

