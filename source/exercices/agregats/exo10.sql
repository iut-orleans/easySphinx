uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select refProd, intituleProd 
	   from PRODUIT natural join CONTENIR natural join CATEGORIE
	   where nomCat='Alimentaire'
	   group by refProd, intituleProd
	   having sum(qte*prixUnitaireProd)>=200
	   order by refProd"

