uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select intituleProd
	   from PRODUIT natural join CATEGORIE
	   where nomCat='Alimentaire' and prixUnitaireProd =(
      	   	 select max( prixUnitaireProd )
      	  	 from PRODUIT natural join CATEGORIE
		 where nomCat='Alimentaire')
	   order by intituleProd"

