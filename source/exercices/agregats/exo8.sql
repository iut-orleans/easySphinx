uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select idRayon, nomRayon, max(prixUnitaireProd) prixMax
	   from RAYON natural join CONTENIR natural join PRODUIT
	   natural join CATEGORIE
	   where nomCat='Alimentaire'
	   group by idRayon, nomRayon
	   order by idRayon"

