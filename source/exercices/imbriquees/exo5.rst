Produits chers
--------------

Donner le nom du produit le plus cher de la base de données  (il  peut  y  en  avoir plusieurs). On veut le résultat trié par ordre alphabétique.

.. easypython:: exercices/imbriquees/exo5.sql
   :language: mysql
   :uuid: 1231313
   :titre: Rayon bazar
