Produits du rayon bazar
-----------------------

Donner la liste des produits du rayon Bazar qui valent moins de 5 Euros.
On veut les références et les intitulés triés par références.

.. easypython:: exercices/imbriquees/exo1.sql
   :language: mysql
   :uuid: 1231313
   :titre: Rayon bazar
