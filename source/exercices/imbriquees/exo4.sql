uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select refProd, intituleProd
from PRODUIT
where refProd in (
     select C1.refProd
     from CONTENIR C1, CONTENIR C2
     where C1.refProd=C2.refProd and C1.idRayon!=C2.idRayon
)
order by refProd"

