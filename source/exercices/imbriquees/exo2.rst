Produits non stockés
--------------------

Donner la liste des produits qui ne sont stockés dans aucun rayon.
On veut la référence et l'intitulé des produits triés par référence.

.. easypython:: exercices/imbriquees/exo2.sql
   :language: mysql
   :uuid: 1231313
   :titre: Produits non stockés
