uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select distinct nomRayon 
from RAYON natural join CONTENIR
where refProd IN (
   select refProd
   from PRODUIT
   where prixUnitaireProd >=ALL(
         select prixUnitaireProd
         from PRODUIT
   )
)
order by nomRayon"

