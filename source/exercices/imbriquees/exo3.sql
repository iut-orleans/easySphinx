uri: 'mysql+pymysql://exerciseur:exerciseur@192.168.13.235/dbexerciseur'

script: ""

solution: "select nomRayon from RAYON
where idRayon not in (
    select idRayon
    from CONTENIR natural join PRODUIT natural join CATEGORIE
    where nomCat!='Alimentaire'
)
order by nomRayon"

