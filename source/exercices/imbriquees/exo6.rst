Rayons chers
------------

Donner le nom de(s) rayon(s) qui stocke(nt) le produit le plus cher de la base de données. On les veut classés dans l'ordre alphabétique. 

.. easypython:: exercices/imbriquees/exo6.sql
   :language: mysql
   :uuid: 1231313
   :titre: Rayons chers
