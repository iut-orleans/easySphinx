Rayons Alimentaires
--------------------

Donner la liste des rayons qui ne stockent que des produits alimentaires.
On veut le nom des rayons triés par ordre alphabétique.

.. easypython:: exercices/imbriquees/exo3.sql
   :language: mysql
   :uuid: 1231313
   :titre: Rayons Alimentaires
