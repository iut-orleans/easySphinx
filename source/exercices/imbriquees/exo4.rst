Produits stockés deux fois
--------------------------

Donner la liste des produits qui sont stockés dans au moins deux rayons différents. On veut les références et les intitulés triés par références.

.. easypython:: exercices/imbriquees/exo4.sql
   :language: mysql
   :uuid: 1231313
   :titre: Rayon bazar
