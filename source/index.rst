.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur EasySQL
=====================

Les sujets:

.. toctree::
   :maxdepth: 2

   td2
   td3
   td4
